from django.http.response import HttpResponse
from django.shortcuts import render
from django.template import loader

from todo_app.models import TodoList

# Create your views here.
def index(request):
    items = TodoList.objects.order_by('-title')[:5]
    template = loader.get_template('todo_app/home.html')
    context = {
        'todo_list': items,
    }
    return HttpResponse(template.render(context, request))
