from django.db import models

# Create your models here.
class TodoList(models.Model):
    title = models.CharField(max_length=100)
    def __str__(self):
        return self.title

class TodoListItem(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField()
    due_date = models.DateField()
    todo_list = models.ForeignKey(TodoList, on_delete=models.CASCADE)

    def __str__(self):
        return self.title
