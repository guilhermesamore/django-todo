# Django Todo Application

## Setup for Windows
1. Clone the repo
2. Open a terminal with access to virtualenv and python
3. Navigate to the root of the project
4. Create a config.bat file in the root
    1. Set the DJANGO_PWD environment variable with the django password
    2. Set the PGPASSWORD environment variable with the postgres user password. This is used when running the postgresql setup file
5. Run setup.bat to create the virtual environment and activate it.

### Optional
1. Run the db-setup.bat to automatically create a DB called django-db with the correct permissions
    * Note, this requires that PGPASSWORD be set and that psql be on the path.
    * Warning: This will flush any existing database called djang-db and a user called django. This will flush any data already added to the DB
    * Warning: This uses the default password django for the django-db. Consider changing after setup

## Running Django for Development
Call `python manage.py runserver` to run the development server
>>>>>>> 629d001 (Add Dirk's template.)
